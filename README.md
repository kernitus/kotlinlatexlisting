# Kotlin LaTeX Listing

LaTeX code listing for syntax Kotlin highlighting. Originally taken from [here](https://github.com/cansik/kotlin-latex-listing) but expanded and improved for other language key words.


Simply place the listing definition at the top of your document:
```latex
\usepackage[dvipsnames]{xcolor}
\usepackage{listings}

\lstdefinelanguage{Kotlin}{
  comment=[l]{//},
  commentstyle={\color{gray}\ttfamily},
  emph={delegate, filter, first, firstOrNull, forEach, lazy, map, mapNotNull, println, return@},
  emphstyle={\color{OrangeRed}},
  identifierstyle=\color{black},
  keywords={abstract, actual, as, as?, break, by, class, inner, companion, continue, data, do, dynamic, else, enum, expect, false, final, for, fun, get, if, import, in, !in, until, interface, internal, is, !is, null, object, override, package, private, public, return, set, super, suspend, this, throw, true, try, typealias, val, var, vararg, when, where, while, typeof},
  keywordstyle={\color{NavyBlue}\bfseries},
  morecomment=[s]{/*}{*/},
  morestring=[b]",
  morestring=[s]{"""*}{*"""},
  ndkeywords={@Deprecated, @JvmField, @JvmName, @JvmOverloads, @JvmStatic, @JvmSynthetic, @Synchronized, Array, Byte, Double, Float, Int, Integer, Iterable, Long, Runnable, Short, String},
  ndkeywordstyle={\color{BurntOrange}\bfseries},
  sensitive=true,
  stringstyle={\color{ForestGreen}\ttfamily},
}
```

Then define your listings' language as Kotlin:
```latex
\begin{lstlisting}[caption={Your code listing}, label={lst:example}, language=Kotlin]
// An example of a code listing
println("Hello there!")
\end{lstlisting}
```
